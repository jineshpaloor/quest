### Quest


#### Setting up the project
* `git clone git@gitlab.com:jineshpaloor/quest.git && cd quest`
* create virtualenv using [virtualenvwrapper](https://virtualenvwrapper.readthedocs.io/en/latest/)
* `pip3 install -r requirements.txt`
* install postgres (`brew install postgres` / `sudo apt-get install postgresql`)
* install Memcached (`brew install memcached` / `sudo apt-get update && sudo apt-get install memcached` )
* create a database and provide permission to user
    * `create database quest;`
    * `GRANT ALL PRIVILEGES ON DATABASE quest TO user;`
* `cp quest/settings/local.py.sample quest/settings/local.py`
* `./manage.py migrate`
* `./manage.py test`
* `./manage.py runserver`


### Dependencies
* Python 3.6
* postgres 9.6.3
* memcached 1.5.7


### API's

* [GET] /api/questions/
    * Return list of questions and it's answers
* [POST] /api/questions/
    * Create a question.
    * payload {'title': 'Question title?', 'private': false, 'asker': 1}
* [GET] /api/questions
    * Return list of questions and it's answers

API is documented at [/docs/](https://infinite-headland-81331.herokuapp.com/docs/)