FROM python:3.7
ENV PYTHONUNBUFFERED 1

ADD ./docker/prepare_image.sh .
RUN ./prepare_image.sh

RUN mkdir /opt/dot
WORKDIR /opt/dot
COPY requirements.txt /opt/dot/requirements.txt
RUN pip install -r requirements.txt

COPY . /opt/dot/
#RUN chmod +x /opt/dot/docker/docker-entrypoint.sh
