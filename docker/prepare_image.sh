#!/bin/bash -xe
apt-get update
apt-get -y install jq less tree zip unzip && \
     pip3.7 install --upgrade pip && \
     pip3.7 install --upgrade awscli && \
     pip3.7 install --upgrade boto3 && \
     apt-get -y autoremove && \
     rm -rf /var/cache/apt/*
apt-get install -y supervisor && \
    rm -rf /var/lib/apt/lists/* && \
    sed -i 's/^\(\[supervisord\]\)$/\1\nnodaemon=true/' /etc/supervisor/supervisord.conf