#!/bin/bash
[ -e /opt/dot/celerybeat-schedule.db ] && rm /opt/dot/celerybeat-schedule.db
pip install --upgrade pip
cat <(echo "yes") - | python /opt/dot/manage.py collectstatic

cp /opt/dot/deploy/app.conf /etc/supervisor/conf.d/app.conf
#cp /opt/dot/deploy/gravity_celery.conf /etc/supervisor/conf.d/gravity_celery.conf
#cp /opt/dot/deploy/gravity_scheduler.conf /etc/supervisor/conf.d/gravity_scheduler.conf

supervisord -c /etc/supervisor/supervisord.conf
