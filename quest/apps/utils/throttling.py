import datetime

from django.core.cache import cache
from rest_framework.throttling import ScopedRateThrottle

from rest_framework.views import exception_handler
from rest_framework.exceptions import Throttled


def custom_exception_handler(exc, context):
    # Call REST framework's default exception handler first,
    # to get the standard error response.
    response = exception_handler(exc, context)

    if isinstance(exc, Throttled):
        custom_response_data = {
            'message': 'request limit exceeded',
            'availableIn': f'{exc.wait} seconds'
        }
        response.data = custom_response_data

    return response


class ThrottleMiddleWare:
    def __init__(self, get_response):
        self.get_response = get_response
        # One-time configuration and initialization.

    def __call__(self, request):
        # Code to be executed for each request before
        # the view (and later middleware) are called.

        response = self.get_response(request)

        # Code to be executed for each request/response after
        # the view is called.
        if request.user.is_authenticated:
            user_id = request.user.pk
        else:
            user_id = 0
        tenant_cache_key = f'request_count_{user_id}'
        tenant_request_count = cache.get(tenant_cache_key, 0)
        cache.set(tenant_cache_key, tenant_request_count + 1)

        return response


class BurstRateThrottle(ScopedRateThrottle):
    scope = 'burst'

    def allow_request(self, request, view):
        """
        Implement the check to see if the request should be throttled.

        On success calls `throttle_success`.
        On failure calls `throttle_failure`.
        """
        if self.rate is None:
            return True

        self.key = self.get_cache_key(request, view)
        if self.key is None:
            return True

        self.history = self.cache.get(self.key, [])
        self.now = self.timer()

        # Set total request count per day
        today_str = datetime.date.today().strftime("%Y_%d_%m")
        self.date_key = f'{self.key}_{today_str}'
        today_request_count = self.cache.get(self.date_key, 0)
        self.cache.set(self.date_key, today_request_count+1)

        # Drop any requests from the history which have now passed the
        # throttle duration
        while self.history and self.history[-1] <= self.now - self.duration:
            self.history.pop()
        if len(self.history) >= self.num_requests and today_request_count >= 100:
            return self.throttle_failure()
        return self.throttle_success()
