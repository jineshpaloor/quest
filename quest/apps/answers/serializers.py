from rest_framework import serializers

from .models import Answers


class AnswersListSerializer(serializers.ModelSerializer):
    provider = serializers.StringRelatedField()

    class Meta:
        model = Answers
        fields = ('id', 'response', 'provider')


class AnswersSerializer(serializers.ModelSerializer):

    class Meta:
        model = Answers
        fields = ('question', 'response', 'provider')
