from django.db import models
from django.contrib.auth.models import User

from quest.apps.utils.models import BaseModel


class Answers(BaseModel):
    question = models.ForeignKey('questions.Questions', related_name='answers', on_delete=models.CASCADE)
    response = models.CharField(max_length=300)
    provider = models.ForeignKey(User, on_delete=models.CASCADE)

    class Meta:
        ordering = ['created_at']

    def __unicode__(self):
        return f"{self.question.title} - {self.response}"

    def __str__(self):
        return f"{self.question.title} - {self.response}"
