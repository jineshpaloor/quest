import uuid

from django.test import TestCase
from django.urls import reverse
from django.contrib.auth.models import User
from rest_framework.authtoken.models import Token

from quest.apps.tenants.models import Tenants
from quest.apps.answers.models import Answers
from quest.apps.questions.models import Questions


class BaseTestView(TestCase):
    def setUp(self):
        # Create a test users
        user = User.objects.create(username='testuser', password=uuid.uuid4().hex)
        user1 = User.objects.create(username='testuser1', password=uuid.uuid4().hex)
        user2 = User.objects.create(username='testuser2', password=uuid.uuid4().hex)

        # Create a Tenant who consumes the API's
        tenant_user = User.objects.create(username='tenant_user', password=uuid.uuid4().hex)
        Tenants.objects.create(name=tenant_user.username)

        question = Questions.objects.create(
            title="Which is the tallest building in the world?",
            private=False,
            asker=user
        )
        Answers.objects.create(question=question, response='Burl Khalifa', provider=user1)
        Answers.objects.create(question=question, response='Petronas Tower', provider=user2)

        # private question
        Questions.objects.create(
            title="What is your age?",
            private=True,
            asker=user1
        )


class QuestionsListViewTests(BaseTestView):

    def test_api_call_without_auth_token(self):
        """
        If authorization token is not provided, user will receive unauthorized response
        """
        response = self.client.get(reverse('questions-list'))
        self.assertEqual(response.status_code, 401)

    def test_api_call_with_token_success_response(self):
        """
        If authorization token is provided, response will be 200
        """
        token = Token.objects.all()[0]
        auth_headers = {
            'HTTP_AUTHORIZATION': 'Token ' + token.key,
        }
        response = self.client.get(reverse('questions-list'), **auth_headers)
        self.assertEqual(response.status_code, 200)
        self.assertEqual(len(response.data), Questions.objects.filter(private=False).count())

    def test_success_response_with_question_attributes(self):
        token = Token.objects.all()[0]
        auth_headers = {
            'HTTP_AUTHORIZATION': 'Token ' + token.key,
        }
        response = self.client.get(reverse('questions-list'), **auth_headers)

        for question in response.data:
            self.assertIn("id", question, "Question id not found")
            self.assertIn("title", question, "Question title not found")
            self.assertIn("answers", question, "Answer key not found")

    def test_private_questions_are_not_part_of_response(self):
        private_question = Questions.objects.filter(private=True)[0]
        token = Token.objects.all()[0]
        auth_headers = {
            'HTTP_AUTHORIZATION': 'Token ' + token.key,
        }
        response = self.client.get(reverse('questions-list'), **auth_headers)
        question_id_list = [q['id'] for q in response.data]

        self.assertNotIn(private_question.id, question_id_list, "Private Question Found in API Response")


class DashboardViewTests(BaseTestView):

    def test_dashboard(self):
        response = self.client.get('/')
        self.assertIn('questions_count', response.context)
        self.assertIn('answers_count', response.context)
        self.assertIn('tenants_count', response.context)
        self.assertIn('users_count', response.context)
