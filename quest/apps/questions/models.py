from django.db import models
from django.contrib.auth.models import User

from quest.apps.utils.models import BaseModel


class Questions(BaseModel):
    title = models.CharField(max_length=300)
    private = models.BooleanField(default=False)
    asker = models.ForeignKey(User, on_delete=models.CASCADE)

    def __unicode__(self):
        return f"{self.title} - {self.asker.username}"

    def __str__(self):
        return f"{self.title} - {self.asker.username}"
