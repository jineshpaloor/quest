from django.contrib import admin

from quest.apps.answers.models import Answers
from .models import Questions


class AnswersInline(admin.TabularInline):
    model = Answers
    extra = 1
    fields = ('response', 'provider')


class QuestionsAdmin(admin.ModelAdmin):
    fields = ('title', 'private', 'asker')
    inlines = [AnswersInline]

admin.site.register(Questions, QuestionsAdmin)

