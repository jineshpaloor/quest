from rest_framework import serializers

from quest.apps.answers.serializers import AnswersListSerializer

from .models import Questions


class QuestionsListSerializer(serializers.ModelSerializer):
    answers = AnswersListSerializer(many=True, read_only=True)

    class Meta:
        model = Questions
        fields = ('id', 'title', 'answers')


class QuestionsSerializer(serializers.ModelSerializer):

    class Meta:
        model = Questions
        fields = ('title', 'asker', 'private')
