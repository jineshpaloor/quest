from django.shortcuts import render
from django.views import View
from django.contrib.auth.models import User

from rest_framework.views import APIView
from rest_framework.response import Response
from rest_framework.authentication import TokenAuthentication, SessionAuthentication
from rest_framework.permissions import IsAuthenticated
from rest_framework import status

from quest.apps.answers.models import Answers
from quest.apps.tenants.models import Tenants

from .models import Questions
from .serializers import QuestionsListSerializer, QuestionsSerializer


class QuestionsView(APIView):
    """
    get:
    Return list of questions and it's answers.

    post:
    Create a question.
    """
    authentication_classes = (TokenAuthentication, SessionAuthentication)
    permission_classes = (IsAuthenticated,)
    throttle_scope = 'questions'

    def get(self, request, format=None):
        query_term = request.query_params.get('query_term', '')
        if query_term:
            questions = Questions.objects.filter(title__icontains=query_term, private=False)
        else:
            questions = Questions.objects.filter(private=False)
        serializer = QuestionsListSerializer(questions, many=True)
        return Response(serializer.data)

    def post(self, request, format=None):
        serializer = QuestionsSerializer(data=request.data)
        if serializer.is_valid():
            serializer.save()
            return Response(serializer.data, status=status.HTTP_201_CREATED)
        return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)


class DashboardView(View):
    template_name = 'dashboard.html'

    def get(self, request):
        users_count_including_tenants = User.objects.all().count()
        questions_count = Questions.objects.all().count()
        answers_count = Answers.objects.all().count()

        Tenants.objects.update_request_count()
        tenants = Tenants.objects.all()

        return render(request, self.template_name, {
            'questions_count': questions_count,
            'answers_count': answers_count,
            'tenants_count': tenants.count(),
            'users_count': users_count_including_tenants - tenants.count(),
            'tenants': tenants
        })
