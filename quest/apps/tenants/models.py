import uuid

from django.db import models
from django.core.cache import cache
from django.contrib.auth.models import User

from rest_framework.authtoken.models import Token


class TenantsManager(models.Manager):

    def update_request_count(self):
        tenants_list = Tenants.objects.all().values_list('id', 'api_key__user_id')
        for tenant_id, user_id in tenants_list:
            tenant_cache_key = f'request_count_{user_id}'
            requests_count = cache.get(tenant_cache_key, 0)
            if requests_count > 0:
                Tenants.objects.filter(id=tenant_id).update(
                    requests_count=models.F('requests_count') + int(requests_count)
                )
                cache.set(tenant_cache_key, 0)


class Tenants(models.Model):
    api_key = models.OneToOneField(Token, on_delete=models.CASCADE)
    name = models.CharField(max_length=30)
    requests_count = models.IntegerField(default=0)

    objects = TenantsManager()

    def __unicode__(self):
        return f"{self.api_key}"

    def __str__(self):
        return f"{self.api_key}"

    def save(self, *args, **kwargs):
        try:
            user = User.objects.get(username=self.name)
            Token.objects.filter(user=user).delete()
        except User.DoesNotExist:
            user = User.objects.create(username=self.name, password=uuid.uuid4().hex)

        self.api_key = Token.objects.create(user=user)

        super(Tenants, self).save(*args, **kwargs)
