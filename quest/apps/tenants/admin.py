from django.contrib import admin

from .models import Tenants


class TenantsAdmin(admin.ModelAdmin):
    fields = ('name',)
    list_display = ('name', 'api_key')

admin.site.register(Tenants, TenantsAdmin)
